##
import os
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import math as ma

os.chdir("C:\\Users\\bguillau\\desktop\\images")
print(os.listdir())

images=[plt.imread(k) for k in ["lena.png","fleur.png","glace.png"]]


def TypeImage(image):
    dim = np.shape(image)
    if len(dim) == 2:
        typeimg="N&B"
    elif dim[2]==3:
        typeimg="RGB"
    else:
        typeimg="RGBA"
    return(typeimg,dim[0:2])

def Affichage(image):
    dim = np.shape(image)
    plt.figure()
    if len(dim)==2:
        plt.imshow(image,cmap=cm.gray)
    else:
        plt.imshow(image)
    plt.axis('off')
    plt.show()
##
def AffichageComposantes(image):
    plt.figure()
    if TypeImage(image)[0]=='N&B':
        plt.imshow(image,cmap=cm.gray)
        plt.title('Composante N')
    else:
        if TypeImage(image)[0]=="RGB" or \
        (TypeImage(image)[0][3]=='A' and image[:,:,3].min()==1):
            L,C,A=1,3,0
        else:
             L,C,A=2,2,1
        plt.subplot(L,C,1)
        plt.imshow(image[:,:,0],cmap=cm.Reds)
        plt.title('Composante R')
        plt.subplot(L,C,2)
        plt.imshow(image[:,:,1],cmap=cm.Greens)
        plt.title('Composante G')
        plt.subplot(L,C,3)
        plt.imshow(image[:,:,2],cmap=cm.Blues)
        plt.title('Composante B')
        if A==1:
            plt.subplot(L,C,4)
            plt.imshow(image[:,:,3],cmap=cm.Greys)
            plt.title('Composante alpha')
    plt.show()

def Resolution(image,N):
    dim = np.shape(image)
    newl = int(dim[0]/N)
    newc = int(dim[1]/N)
    if len(dim)==2:
        newimg=np.zeros((newl,newc))
        for i in range(newl):
            for j in range(newc):
                value=0
                for k in range(N):
                    for l in range(N):
                        value+=image[N*i+k,N*j+l]
                newimg[i,j] =value/N**2
    else:
        ncolor=dim[2]
        newimg=np.zeros((newl,newc,ncolor))
        for i in range(newl):
            for j in range(newc):
                for color in range(ncolor):
                    value=0
                    for k in range(N):
                        for l in range(N):
                            value+=image[N*i+k,N*j+l,color]
                    newimg[i,j,color]=value/N**2
    return(newimg)
